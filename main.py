from openweather import GeocodingRequest, GeoCodingParams, Location, forecast_16_days
from openweather import forecast_16_days
from output import PrettyTablePrinter
import os
import sys


def main():
    """Runs the function"""

    # default if not returned
    no_value = "ERROR_NO_VALUE"

    api_key = os.getenv("OPENWEATHER", no_value)
    city = os.getenv("CITY", no_value)
    state = os.getenv('STATE', no_value)
    country = os.getenv('COUNTRY', no_value)

    if no_value in [ api_key, city, state, country]:
        print(f"You will need to provide environment variables for all the parameters")
        print(f"refer to the readme.")
        sys.exit(1)


    g_params: GeoCodingParams = GeoCodingParams(
                        city=city,
                        state=state,
                        country=country,
                        openweather_api_key=api_key)

    g_request: GeocodingRequest = GeocodingRequest(geocoding_params=g_params)

    openweather_location: Location = g_request.get_coordinates()
    
    forecast_parameters = forecast_16_days.Parameters(
                            location=openweather_location,
                            api_key=api_key)
    
    f16 = forecast_16_days.Request(parameters=forecast_parameters)
    forecast_data = f16.get_forecast()
    printer = PrettyTablePrinter(forecast_data=forecast_data, location=openweather_location)
    printer.print()

if __name__ == "__main__":
    main()