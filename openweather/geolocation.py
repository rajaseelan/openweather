from dataclasses import dataclass, field
from typing import Dict, Union

import requests
from . exceptions import ConfigurationError,NetworkError,OpenWeatherError

@dataclass(kw_only=True, frozen=True)
class Location:
    """"Coordinates for OpenWeather location. To be used in actual queries"""
    name: str
    lat: float
    lon: float
    country: str
    state: str


@dataclass(kw_only=True, frozen=True)
class GeoCodingParams:
    """Geocoding API Input"""
    city: str
    state: str
    country: str
    openweather_api_key: str
    limit: int = field(init=False, default=1)

    def params(self) -> Dict[str, Union[str, int]]:
        """Parameters supplied to requests.get() params keyword"""
        return {
            "q": ",".join([self.city,self.state,self.country]),
            "limit": self.limit,
            "appid": self.openweather_api_key
        }


class GeocodingRequest:
    """Get Lat / long of the Location to issue a weather request"""

    api_endpoint = "http://api.openweathermap.org/geo/1.0/direct"


    def __init__(self, geocoding_params: GeoCodingParams) -> None:
        self.geocoding_params = geocoding_params
    
    def get_coordinates(self) -> Union[Location, Exception]:
        try:
            response = requests.get(url=self.api_endpoint, params=self.geocoding_params.params())

            if response.status_code == 401:
                raise ConfigurationError(f"There is something wrong with the API Key")

            if  response.status_code == 404:
                raise ConfigurationError(f"You input data could be wrong.")

            if response.status_code == 429:
                raise ConfigurationError(f"Over API rate limit at the moment")

            if response.status_code in [ 500, 502, 503, 504 ]:
                raise OpenWeatherError(f"Internal Server Error in provider's side.")
            
            return self._extract_coordinates(response)

        except ConnectionRefusedError as e:
            raise NetworkError(f"Unable to connect to {self.api_endpoint}")

    def _extract_coordinates(self, response: requests.Response) -> Location:
        """Extract the coordinates from the successful openweather response"""
        response_json = response.json()
        coordnates = response_json[-1]
        
        return Location(
            name = coordnates["name"],
            state = coordnates["state"],
            country = coordnates["country"],
            lat = coordnates["lat"],
            lon = coordnates["lon"]
        )