"""16 Day Weather Forecast API"""

from dataclasses import dataclass,field
from typing import Union
import requests

from .geolocation import Location
from .exceptions import NetworkError,OpenWeatherError,ConfigurationError


@dataclass(kw_only=True, frozen=True)
class Parameters:
    """Request to the weather api that returns the 16 day forecast for a location"""
    location: Location
    api_key: str
    count: int = field(init=False, default=16)
    mode: str = field(init=False, default="json")
    units: str = field(init=False, default="metric")
    lang: str = field(init=False, default="en")

    @property
    def longitude(self) -> float:
        return self.location.lon
    
    @property
    def latitude(self) -> float:
        return self.location.lat
    
    def params(self) -> dict[Union[float, str, int]]:
        """Return the parameters as a Request.get() params argument"""

        return {
            "lat": self.latitude,
            "lon": self.longitude,
            "cnt": self.count,  
            "appid": self.api_key,
            "mode": self.mode,
            "units": self.units,
            "lang": self.lang
        }


@dataclass(kw_only=True, frozen=True)
class TemperatureForecast:
    """Forecast for one day"""
    morning: float
    evening: float
    night: float
    timestamp: int


class Request:
    """Get the 16 Day forecast of a properly defined location in the Openweather API"""

    api_endpoint = "http://api.openweathermap.org/data/2.5/forecast/daily"

    def __init__(self, parameters: Parameters):
        self.parameters = parameters

    def get_forecast(self) -> dict[int,TemperatureForecast]:

        api_response = None

        try:
            api_response = self._call_openweather_endpoint()
        except NetworkError as e:
            raise NetworkError(repr(e))
        except ConfigurationError as e:
            raise ConfigurationError(repr(e))
        except OpenWeatherError as e:
            raise OpenWeatherError(repr(e))

        forecast_data = self._filter_results_for_16_day_forecast(api_response)
        
        return forecast_data

    def _call_openweather_endpoint(self) -> Union[requests.Response, None]:
        """Call the OpenWeather API Endpoint and get the forecast"""
  
        try:
            response = requests.get(self.api_endpoint, params=self.parameters.params())
            if response.status_code == 401:
                raise ConfigurationError(f"There is something wrong with the API Key")

            if  response.status_code == 404:
                raise ConfigurationError(f"You input data could be wrong.")

            if response.status_code == 429:
                raise OpenWeatherError(f"Over API rate limit at the moment")

            if response.status_code in [ 500, 502, 503, 504 ]:
                raise OpenWeatherError(f"Internal Server Error in provider's side.")
            
            return response

        except ConnectionRefusedError as e:
            raise NetworkError(f"Unable to connect to {self.api_endpoint}")
    
    def _filter_results_for_16_day_forecast(self, response: requests.Response) -> dict[int, TemperatureForecast]:
        parser = ResponseParser(response=response)
        forecast_data = parser.parse()
        return forecast_data


class ResponseParser:
    """Parses the OpenWeather Reponse into a dict of Morning / Evening / Night Data"""

    def __init__(self, response: requests.Response):
        self.response = response

    def parse(self) -> dict[int, TemperatureForecast]:
        """Parse the Openweather Response"""

        forecast_data = dict()

        returned_data = self.response.json()
        if returned_data["cnt"] == 0:
            # no forecast data, return an empty dict
            return forecast_data
        
        if returned_data["cnt"] > 0:
            forecasts = returned_data["list"]
            for entry in forecasts:
                timestamp = entry["dt"]
                morning_temperature = entry["temp"]["morn"]
                evening_temprature = entry["temp"]["eve"]
                night_temperature = entry["temp"]["night"]

                tf = TemperatureForecast(
                        timestamp=timestamp,
                        morning=morning_temperature,
                        evening=evening_temprature,
                        night=night_temperature)
                
                forecast_data[timestamp] = tf
        
        return forecast_data







