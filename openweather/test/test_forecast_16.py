"""Testing the Forecast 16 Module"""

import pytest
import requests
from openweather.geolocation import Location
from openweather.forecast_16_days import Parameters


@pytest.fixture
def mock_location():
    """Mock Location for Forecast16DaysParameters"""

    return Location(
            name="Woking",
            lat=51.3201891,
            lon=-0.5564726,
            country="GB",
            state="England")

@pytest.fixture
def mock_openweather_api_key():
    """Mock OpenWeather API Key for Parameters"""
    return "WEWEWEWEWEWEWE"

def test_forecast_16_days_parameters(mock_location, mock_openweather_api_key):
    """Test the variables input into the Forecast16DaysParameters"""

    f = Forecast16DaysParameters(
                                location=mock_location,
                                api_key=mock_openweather_api_key)

    assert f.latitude == mock_location.lat
    assert f.longitude == mock_location.lon
