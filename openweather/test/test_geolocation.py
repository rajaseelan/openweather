import pytest
import requests
from openweather.geolocation import GeoCodingParams,GeocodingRequest,Location


@pytest.fixture
def geocoding_params():
    """A set of mock data for the geocoding parameters"""
    return {
        "city": "Woking",
        "state": "England",
        "country": "GB",
        "fake_api_key": "WEWEWEWEWEWE"
    }


def test_geocoding_params(geocoding_params):
    """ Test the GeocodingParams initialiation"""

    g = GeoCodingParams(
        city = geocoding_params["city"],
        state = geocoding_params["state"],
        country = geocoding_params["country"],
        openweather_api_key = geocoding_params["fake_api_key"]
    )

    assert g.city == geocoding_params["city"]
    assert g.state == geocoding_params["state"]
    assert g.country == geocoding_params["country"]
    assert g.openweather_api_key == geocoding_params["fake_api_key"]


def test_geocoding_params_function(geocoding_params):
    """test the input supplied to the requests.get *params* kwarg"""

    g = GeoCodingParams(
        city = geocoding_params["city"],
        state = geocoding_params["state"],
        country = geocoding_params["country"],
        openweather_api_key = geocoding_params["fake_api_key"]
    )

    # just testing how it assembles the query the location
    assert g.params()["q"] == ",".join([geocoding_params["city"],
                                     geocoding_params["state"],
                                     geocoding_params["country"]])


class MockResponse:
    """Mocks the response of a successfull call to the Geocoding API"""
    
    status_code = 200

    @staticmethod
    def json():
        return [
            {'name': 'Woking',
            'lat': 51.3201891,
            'lon': -0.5564726,
            'country': 'GB',
            'state': 'England'}]


@pytest.fixture
def expected_location():
    return Location(
        name = 'Woking',
        state = 'England',
        country = 'GB',
        lat = 51.3201891,
        lon = -0.5564726
    )


def test_succsfull_call(monkeypatch, geocoding_params, expected_location):
    """Test the results of a successfull call to the Geocoding API"""
    
    # setup a list of parameters to supply to the GeoCodingRequest
    parameters = GeoCodingParams(
        city = geocoding_params["city"],
        state = geocoding_params["state"],
        country = geocoding_params["country"],
        openweather_api_key = geocoding_params["fake_api_key"]
    )

    def mock_get(*args, **kwargs):
        return MockResponse()

    monkeypatch.setattr(requests, 'get', mock_get, )

    g_request = GeocodingRequest(geocoding_params=parameters)
    received_location = g_request.get_coordinates()

    assert type(received_location) == type(expected_location)
    assert received_location.lon == expected_location.lon
    assert received_location.lat == expected_location.lat
