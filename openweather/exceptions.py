"""Exceptions for this module"""

class ConfigurationError(Exception):
    """Errors returned by successful API calls that indicate
       the parameters supplied were not correct"""
    pass


class NetworkError(Exception):
    """Network Errors encountered while trying to connect to
       Endpoint"""
    pass


class OpenWeatherError(Exception):
    """Internal Server Errors from the OpenWeather Endppoints"""
    pass

