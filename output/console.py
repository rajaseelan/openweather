"""Console Output for the 16 Day Weather Forecast"""

from typing import List,Tuple
from datetime import datetime
import prettytable

from openweather.forecast_16_days import TemperatureForecast
from openweather.geolocation import Location


class PrettyTablePrinter:
    """Output to Console using PrettyTable Library"""

    def __init__(self, forecast_data: dict[int, TemperatureForecast], location: Location):
        self.location = location
        self.forecast_data: dict[int, TemperatureForecast] = forecast_data
        self.ordered_timestamps: List[int] = self._sort_timestamps_descending()

    def print(self) -> None:
        """Prints table to stdout"""

        # required data
        months: List[str] = self._helper_extract_month()
        dates: List[int] = self._helper_extract_dates()
        morning, evening, night = self._helper_extract_temperature()
        
        self._print_title(months)

        table: prettytable.PrettyTable = prettytable.PrettyTable()

        # table.field_names = headers
        table.add_column("Date", dates)
        table.add_column("Morning", morning)
        table.add_column("Evening", evening)
        table.add_column("Night", night)

        print(table)        

    def _print_title(self, months: List[str]):
        """Print the title of the commoand line program"""

        location_str = f"{self.location.name}, {self.location.state}, {self.location.country}"

        print(f"Location: {location_str}")
        if len(months) > 1:
            print(f"16 Day Temperature Forecast for {months[0]} and {months[1]}.")
        else:
            print(f"16 Day Temperature Forecast for {months[0]}.")

    def _sort_timestamps_descending(self) -> List[int]:
        """Sort the keys of the forecast_data"""

        keys = list(self.forecast_data.keys())
        keys.sort()
        return keys

    def _helper_extract_month(self) -> List[str]:
        """Extract the month as an Abbreviated short form from all the timestamps"""

        months: List[str] = []

        for timestamp in self.ordered_timestamps:
            month = datetime.utcfromtimestamp(timestamp).strftime("%b")
            if not month in months:
                months.append(month)
        return months

    def _helper_extract_dates(self) -> List[int]:
        """Extract the month as an Abbreviated short form from all the timestamps"""

        dates: List[int] = []

        for timestamp in self.ordered_timestamps:
            date = datetime.utcfromtimestamp(timestamp).day
            dates.append(date)
        return dates

    def _helper_extract_temperature(self) -> Tuple[List[float], List[float], List[float]]:
        """Extract the morning temperature according to date sequence"""
        
        morning: List[float] = []
        evening: List[float] = []
        night: List[float] = []

        for timestamp in self.ordered_timestamps:
            forecast = self.forecast_data[timestamp]
            morning.append(forecast.morning)
            evening.append(forecast.evening)
            night.append(forecast.night)

        return morning, evening, night

    


