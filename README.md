# 16 Day Temperature Forecast for Morning / Evening / Night via data from OpenWeather 

## Overview

This utility prints out a table with a 16 Day temperature forecast of the weather in celsius.

## Using this utility

### API Key
1. Get your api key from [openweathermap.org](https://openweathermap.org/forecast16), you will have to register.

### Location
1. Get your actual location as defined in openweathermap. Refer to the Geocoding API [https://openweathermap.org/api/geocoding-api](https://openweathermap.org/api/geocoding-api)

### Setup Enviroment
This was designed to be eventually run in a docker container, and was tested using `python:3.10.4-bullseye`
1. Clone repo into container
1. Create a virtual enviroment and activate it. `python -m venv myweather && source myweather/bin/activate`
1. Install dependencies: `pip install -r requirements.txt`

### Supply parameters
1. Export your parameters as environment variables
```
# example, follow the key names
export OPENWEATHER="my-secret-api-key"
export CITY="Woking"
export STATE="England"
export COUNTRY="GB"
```

### Run Script
`python3 main.py`

### Sample Output
```
Location: Woking, England, GB
16 Day Temperature Forecast for May and Jun.
+------+---------+---------+-------+
| Date | Morning | Evening | Night |
+------+---------+---------+-------+
|  30  |   6.62  |  13.25  |  9.54 |
|  31  |   8.11  |   14.1  |  8.68 |
|  1   |   9.05  |  15.58  | 11.15 |
|  2   |  11.09  |  18.42  | 13.96 |
|  3   |  13.52  |  17.11  | 13.32 |
|  4   |  10.12  |  18.26  | 11.69 |
|  5   |  11.09  |  17.03  | 11.44 |
|  6   |  12.38  |  20.36  | 14.09 |
|  7   |  13.29  |  18.45  | 15.58 |
|  8   |  16.96  |  20.43  | 13.26 |
|  9   |  12.83  |  17.85  | 12.35 |
|  10  |  11.17  |  18.56  | 12.46 |
|  11  |   11.3  |  19.96  | 17.58 |
|  12  |   15.4  |   19.6  | 12.95 |
|  13  |  12.79  |  20.26  | 13.28 |
|  14  |  12.81  |  20.99  | 13.38 |
+------+---------+---------+-------+
```

